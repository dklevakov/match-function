# my_pattern = str(input())
# my_string = str(input())


def match_string_to_pattern(my_pattern: str, my_string: str) -> bool:
    """
    Функция сопоставляет шаблон со строкой и выдает булево значение - соответствует или нет
    '*' соответствует любой последовательности литер, включая пустую.
    '%' соответствует в точности одной литере.

    """

    i, j = 0, 0
    p = len(my_pattern)
    s = len(my_string)
    while i < p and j < s:

        while i < p and j < s and my_pattern[i] == my_string[j]:
            i += 1
            j += 1
            if i == p and j == s:
                return True

        while i < p and my_pattern[i] == '%':
            i += 1
            j += 1
            if i == p and j == s:
                return True

        if i < p and my_pattern[i] == '*':
            if i == p - 1:
                return True
            i += 1
            while my_string[j] != my_pattern[i]:
                j += 1
                if j == s and i != p - 1:
                    return False

        if i < p and j < s and my_string[j] != my_pattern[i]:
            return False

    return False


# print(match_string_to_pattern('*Abcd', 'rtyuAbcd'))