from main import match_string_to_pattern
import pytest


@pytest.mark.parametrize("my_pattern, my_string", [('*Abcd', 'rtyuAbcd'),
                                                   ('*Abcd', 'Abcd'),
                                                   ('*Abcd', 'кпа23увAbcd'),
                                                   ('A*d', 'Abcd'),
                                                   ('A*d', 'Aвапр45#,:d'),
                                                   ('Abc*', 'Abc'),
                                                   ('A*', 'Acfyvubyi'),
                                                   ('A%%d', 'ABCd'),
                                                   ('%b%d', 'abcd'),
                                                   ('Abc%', 'AbcD'),
                                                   ('%b*d', 'Abcd'),
                                                   ('%%%*', 'Abcd'),
                                                   ('%%%%*', 'Abcd'),
                                                   ('%bc*', 'abc')
])
def test_match_is_true(my_pattern, my_string):
    assert match_string_to_pattern(my_pattern, my_string) is True


@pytest.mark.parametrize("my_pattern, my_string", [('*Abcd', 'rtyuaBCD'),
                                                   ('*Abcd', 'Acd'),
                                                   ('*Abcd', 'кпа23?увAbhcd'),
                                                   ('A*d', 'bcd'),
                                                   ('A*d', 'Ad1'),
                                                   ('Abc*', 'bAc'),
                                                   ('A%%d', 'ABBCd'),
                                                   ('%b%d', 'aBcd'),
                                                   ('Abc%', 'Abc'),
                                                   ('%bc*', 'acbc')])
def test_match_is_false(my_pattern, my_string):
    assert match_string_to_pattern(my_pattern, my_string) is False
